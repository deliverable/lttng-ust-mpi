<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Tutorial

This tutorial shows how to compile the Exatracer and use it for local testing.
Before starting the tutorial, make sure that the Exatracer is built and that all
required dependencies are met by following the [Build](build.md) document.

# User program

The following is the program that will be instrumented, which is the MPI
[hello world example](https://mpitutorial.com/tutorials/mpi-hello-world/).

hello-world.c:
```c
#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {

    int world_size;
    int world_rank;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;

    MPI_Init(NULL, NULL);

    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    MPI_Get_processor_name(processor_name, &name_len);

    printf("Hello world from processor %s, rank %d out of %d processors\n",
           processor_name, world_rank, world_size);

    MPI_Finalize();
}
```

The program is compiled with OpenMPI:
```sh
$ mpicc -o hello-world hello-world.c
```

# Compiling the Exatracer

Since the program is compiled with OpenMPI, the Exatracer configuration is
untouched.  The default provider `mpi` will be used.

```sh
$ make
```

# Running the application

The following script shows how the Exatracer can be used to instrument the `hello-world` program:

```sh
TRACE_OUTPUT=my-traces

# Create an LTTng session and dump the trace to `my-traces' directory.
lttng create --output $TRACE_OUTPUT

# Enable all MPI event types.  For every public function of MPI, there are two
# events.  `enter' and `exit'.
#
# All events have some context information, like timestamp, hostname and CPU ID.
#
# The `enter' event has all the arguments passed to the function, plus a pair of
# IDs that when concatenated, forms a unique ID in the trace.
#
# The `exit' event only has the corresponding pair of IDs to do correlation with
# the `enter' event.
lttng enable-event --userspace 'mpi:*'

# The Exatracer defines a `rank' user application context field, an LTTng concept where an
# application can define contextual fields that may be added to each event.  When
# added, the node rank is added to every event of the trace.
lttng add-context --userspace --type '$app.MPI:rank'

# Starting the session is required to get the trace.
lttng start

# The application is executed locally using `mpirun'.  This makes this tutorial
# easier to write.  For real application with multiple nodes, the previous setup
# with LTTng is not enough.  See the LTTng documentation on how to use it for
# distributed network (https://lttng.org/docs/v2.13/#doc-lttng-relayd).
mpirun --n 4 env LD_PRELOAD=./liblttng-ust-mpi.so ./hello-world

# When done, the tracing session can be destroyed to flush all pending events.
lttng destroy

# The trace can now be observed with babeltrace or Trace Compass.
babeltrace2 $TRACE_OUTPUT
```

# The trace output

The output from Babeltrace should look like this:

```text
[14:37:51.689965564] (+?.?????????) HOST mpi:enter_MPI_Init: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, argc = 0x0, argv = 0x0 }
[14:37:51.698465775] (+0.008500211) HOST mpi:enter_MPI_Init: { cpu_id = 1 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, argc = 0x0, argv = 0x0 }
[14:37:51.698660821] (+0.000195046) HOST mpi:enter_MPI_Init: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, argc = 0x0, argv = 0x0 }
[14:37:51.707388306] (+0.008727485) HOST mpi:enter_MPI_Init: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, argc = 0x0, argv = 0x0 }
[14:37:52.886348924] (+1.178960618) HOST mpi:exit_MPI_Init: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886349584] (+0.000000660) HOST mpi:exit_MPI_Init: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886349974] (+0.000000390) HOST mpi:exit_MPI_Init: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886353584] (+0.000003610) HOST mpi:exit_MPI_Init: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_none" : container = 0 ), _app_MPI_rank = { { } } }, { lttng_thread_id = 0, lttng_local_id = 0, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886386013] (+0.000032429) HOST mpi:enter_MPI_Comm_size: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 1, comm = 0x404040, size = 0x7FFE47A44C1C }
[14:37:52.886386033] (+0.000000020) HOST mpi:enter_MPI_Comm_size: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 1, comm = 0x404040, size = 0x7FFF36B71DFC }
[14:37:52.886386083] (+0.000000050) HOST mpi:enter_MPI_Comm_size: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 1, comm = 0x404040, size = 0x7FFFC951E8DC }
[14:37:52.886386893] (+0.000000810) HOST mpi:exit_MPI_Comm_size: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 1, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886387453] (+0.000000560) HOST mpi:enter_MPI_Comm_size: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 1, comm = 0x404040, size = 0x7FFE9B97AFDC }
[14:37:52.886388203] (+0.000000750) HOST mpi:enter_MPI_Comm_rank: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 2, comm = 0x404040, rank = 0x7FFF36B71DF8 }
[14:37:52.886388253] (+0.000000050) HOST mpi:exit_MPI_Comm_size: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 1, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886388393] (+0.000000140) HOST mpi:exit_MPI_Comm_size: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 1, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886388563] (+0.000000170) HOST mpi:exit_MPI_Comm_size: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 1, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886388693] (+0.000000130) HOST mpi:exit_MPI_Comm_rank: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 2, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886389233] (+0.000000540) HOST mpi:enter_MPI_Comm_rank: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 2, comm = 0x404040, rank = 0x7FFE9B97AFD8 }
[14:37:52.886389363] (+0.000000130) HOST mpi:enter_MPI_Comm_rank: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 2, comm = 0x404040, rank = 0x7FFE47A44C18 }
[14:37:52.886389443] (+0.000000080) HOST mpi:enter_MPI_Comm_rank: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 2, comm = 0x404040, rank = 0x7FFFC951E8D8 }
[14:37:52.886389793] (+0.000000350) HOST mpi:exit_MPI_Comm_rank: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 2, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886390013] (+0.000000220) HOST mpi:exit_MPI_Comm_rank: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 2, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886390033] (+0.000000020) HOST mpi:exit_MPI_Comm_rank: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 2, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886393263] (+0.000003230) HOST mpi:enter_MPI_Get_processor_name: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 3, name = 0x7FFF36B71CF0, resultlen = 0x7FFF36B71CEC }
[14:37:52.886393323] (+0.000000060) HOST mpi:enter_MPI_Get_processor_name: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 3, name = 0x7FFFC951E7D0, resultlen = 0x7FFFC951E7CC }
[14:37:52.886393503] (+0.000000180) HOST mpi:enter_MPI_Get_processor_name: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 3, name = 0x7FFE9B97AED0, resultlen = 0x7FFE9B97AECC }
[14:37:52.886394563] (+0.000001060) HOST mpi:enter_MPI_Get_processor_name: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 3, name = 0x7FFE47A44B10, resultlen = 0x7FFE47A44B0C }
[14:37:52.886398063] (+0.000003500) HOST mpi:exit_MPI_Get_processor_name: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 3, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886398113] (+0.000000050) HOST mpi:exit_MPI_Get_processor_name: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 3, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886398123] (+0.000000010) HOST mpi:exit_MPI_Get_processor_name: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 3, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886398163] (+0.000000040) HOST mpi:exit_MPI_Get_processor_name: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 3, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.886409373] (+0.000011210) HOST mpi:enter_MPI_Finalize: { cpu_id = 9 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 4 }
[14:37:52.886411233] (+0.000001860) HOST mpi:enter_MPI_Finalize: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 4 }
[14:37:52.886414163] (+0.000002930) HOST mpi:enter_MPI_Finalize: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 4 }
[14:37:52.886417023] (+0.000002860) HOST mpi:enter_MPI_Finalize: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 4 }
[14:37:52.929825573] (+0.043408550) HOST mpi:exit_MPI_Finalize: { cpu_id = 3 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 3 } }, { lttng_thread_id = 0, lttng_local_id = 4, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.929858042] (+0.000032469) HOST mpi:exit_MPI_Finalize: { cpu_id = 8 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 0 } }, { lttng_thread_id = 0, lttng_local_id = 4, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.929862972] (+0.000004930) HOST mpi:exit_MPI_Finalize: { cpu_id = 1 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 1 } }, { lttng_thread_id = 0, lttng_local_id = 4, lttng_has_ret = 1, lttng_ret = 0 }
[14:37:52.929976970] (+0.000113998) HOST mpi:exit_MPI_Finalize: { cpu_id = 10 }, { _app_MPI_rank_tag = ( "_int64" : container = 4 ), _app_MPI_rank = { 2 } }, { lttng_thread_id = 0, lttng_local_id = 4, lttng_has_ret = 1, lttng_ret = 0 }
```

Each line represents an event.  Events are printed in chronolical order.  The
anatomy of a event is like so: 

`[TIMESTAMP] (PREVIOUS-DIFF) HOSTNAME PROVIDER:EVENT_NAME CONTEXT ... { FIELDS ... }`

Refer to the [Babeltrace
documentation](https://babeltrace.org/docs/v2.0/man1/babeltrace2-convert.1/) for
interpreting the events.



