<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Building

## Prerequisites

- **[GCC](https://gcc.gnu.org/) >= 7.5.0**
- **[lttng-tools](https://lttng.org/) >= 2.13.11**
- **[lttng-ust](https://lttng.org/) >= 2.13.7**
- **[make](https://www.gnu.org/software/make/) >= 4.3**
- **[openmpi](https://www.open-mpi.org) >= 4.1.6** or **craympi**
- **[pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/) >= 0.29.2**
- **[python-clang](https://clang.llvm.org) >= 13.0.1**

## Optional dependencies

- **[babeltrace](https://babeltrace.org/) >= 2.0.5**

## Build steps

### Configuration

Edit the variables under `config.mk` to configure the project.

### Compilation

Call `make`.  The shared library `liblttng-ust-mpi.so` should now exist.

### Testing

Call `make check` to run the test.  A dummy program is compiled and executed
with either `mpirun` or `srun`.  All events for MPI are enabled and the node
rank is added as a user application context field.  The final trace is then
printed by `babeltrace` onto the console.  The trace can be further analyze
under the `traces` directory.

### Installing

Not yet supported.
