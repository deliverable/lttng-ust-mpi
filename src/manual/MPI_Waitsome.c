#include "common.h"

side_static_event(side_event_enter_MPI_Waitsome, "mpi", "enter_MPI_Waitsome", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_int("requests-count"),
	)
);

side_static_event(side_event_exit_MPI_Waitsome, "mpi", "exit_MPI_Waitsome", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_gather_vla("statuses",
				      side_elem(side_type_gather_struct(side_mpi_status_struct_def,
									0,
									3 * sizeof(int),
									SIDE_TYPE_GATHER_ACCESS_DIRECT)),
				      0, SIDE_TYPE_GATHER_ACCESS_DIRECT,
				      side_length(side_type_gather_signed_integer(0, sizeof(int), 0, 0, SIDE_TYPE_GATHER_ACCESS_DIRECT))),
		side_field_optional_literal("result", side_elem(side_type_s64())),
	)
);

struct side_mpi_api_state_MPI_Waitsome {
	MPI_Status *statuses;
	int *outcount;
	int *indices;
	struct side_mpi_unique_id id;
	int has_ret;
	int ret;
};

static inline void enter_MPI_Waitsome(struct side_mpi_api_state_MPI_Waitsome *side_state,
				      int incount, MPI_Request *requests,
				      int *outcount, int *indices,
				      MPI_Status *statuses)
{
	side_state->statuses = statuses;
	side_state->outcount = outcount;
	side_state->indices  = indices;

	side_mpi_id_generator_next_id(&side_state->id);

	side_event(side_event_enter_MPI_Waitsome,
		side_arg_list(
			side_arg_u64(side_state->id.thread_id),
			side_arg_u64(side_state->id.local_id),
			side_arg_int(incount),
		)
	);
}

static inline void exit_MPI_Waitsome(struct side_mpi_api_state_MPI_Waitsome *side_state)
{
	if (side_event_enabled(side_event_exit_MPI_Waitsome)) {

		int outcount = *side_state->outcount;
		MPI_Status statuses[outcount];

		for (size_t k=0; k<outcount; ++k) {
			size_t i = side_state->indices[k];
			statuses[k] = side_state->statuses[i];
		}

		side_arg_define_optional(result, side_arg_s64(side_state->ret),
					 side_state->has_ret ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_event(side_event_exit_MPI_Waitsome,
			side_arg_list(
				side_arg_u64(side_state->id.thread_id),
				side_arg_u64(side_state->id.local_id),
				side_arg_gather_vla(statuses, &outcount),
				side_arg_optional(result),
			)
		);
	}
}
