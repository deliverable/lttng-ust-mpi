#include "common.h"

side_static_event(side_event_enter_MPI_Testsome, "mpi", "enter_MPI_Testsome", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_int("requests-count"),
	)
);

side_static_event(side_event_exit_MPI_Testsome, "mpi", "exit_MPI_Testsome", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_optional("statuses", side_mpi_maybe_statuses),
		side_field_optional_literal("result", side_elem(side_type_s64())),
	)
);

struct side_mpi_api_state_MPI_Testsome {
	MPI_Status *statuses;
	int *outcount;
	int *indices;
	struct side_mpi_unique_id id;
	int has_ret;
	int ret;
};

static inline void enter_MPI_Testsome(struct side_mpi_api_state_MPI_Testsome *side_state,
				      int incount, MPI_Request *requests,
				      int *outcount, int *indices,
				      MPI_Status *statuses)
{
	side_state->statuses = statuses;
	side_state->outcount = outcount;
	side_state->indices  = indices;

	side_mpi_id_generator_next_id(&side_state->id);

	side_event(side_event_enter_MPI_Testsome,
		side_arg_list(
			side_arg_u64(side_state->id.thread_id),
			side_arg_u64(side_state->id.local_id),
			side_arg_int(incount),
		)
	);
}

static inline void exit_MPI_Testsome(struct side_mpi_api_state_MPI_Testsome *side_state)
{
	if (side_event_enabled(side_event_exit_MPI_Testsome)) {

		int outcount = *side_state->outcount;
		MPI_Status array_of_statuses[outcount];

		for (size_t k=0; k<outcount; ++k) {
			size_t i = side_state->indices[k];
			array_of_statuses[k] = side_state->statuses[i];
		}

		side_arg_define_optional(statuses,
					 side_arg_gather_vla(array_of_statuses, &outcount),
					 outcount > 0 ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_arg_define_optional(result, side_arg_s64(side_state->ret),
					 side_state->has_ret ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_event(side_event_exit_MPI_Testsome,
			side_arg_list(
				side_arg_u64(side_state->id.thread_id),
				side_arg_u64(side_state->id.local_id),
				side_arg_optional(statuses),
				side_arg_optional(result),
			)
		);
	}
}
