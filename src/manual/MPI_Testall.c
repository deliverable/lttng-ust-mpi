side_static_event(side_event_enter_MPI_Testall, "mpi", "enter_MPI_Testall", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
	)
);

side_static_event(side_event_exit_MPI_Testall, "mpi", "exit_MPI_Testall", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_optional("statuses", side_mpi_maybe_statuses),
		side_field_optional_literal("result", side_elem(side_type_s64())),
	)
);

struct side_mpi_api_state_MPI_Testall {
	MPI_Status *statuses;
	struct side_mpi_unique_id id;
	int count;
	int *flag;
	int has_ret;
	int ret;
};

static inline void enter_MPI_Testall(struct side_mpi_api_state_MPI_Testall *side_state,
				     int count, MPI_Request *requests,
				     int *flag, MPI_Status *statuses)
{
	assert(MPI_STATUSES_IGNORE != statuses);

	side_state->count    = count;
	side_state->flag     = flag;
	side_state->statuses = statuses;

	side_mpi_id_generator_next_id(&side_state->id);

	side_event(side_event_enter_MPI_Testall,
		side_arg_list(
			side_arg_u64(side_state->id.thread_id),
			side_arg_u64(side_state->id.local_id),
		)
	);
}

static inline void exit_MPI_Testall(struct side_mpi_api_state_MPI_Testall *side_state)
{
	if (side_event_enabled(side_event_exit_MPI_Testall)) {
		side_arg_define_optional(statuses,
					 side_arg_gather_vla(side_state->statuses, &side_state->count),
					 *side_state->flag ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_arg_define_optional(result, side_arg_s64(side_state->ret),
					 side_state->has_ret ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_event(side_event_exit_MPI_Testall,
			side_arg_list(
				side_arg_u64(side_state->id.thread_id),
				side_arg_u64(side_state->id.local_id),
				side_arg_optional(statuses),
				side_arg_optional(result),
			)
		);
	}
}
