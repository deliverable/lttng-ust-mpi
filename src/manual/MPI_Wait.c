#include "common.h"

side_static_event(side_event_enter_MPI_Wait, "mpi", "enter_MPI_Wait", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
	)
);

side_static_event(side_event_exit_MPI_Wait, "mpi", "exit_MPI_Wait", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_gather_struct("status",
					 side_mpi_status_struct_def,
					 0, 3 * sizeof(int), SIDE_TYPE_GATHER_ACCESS_DIRECT),
		side_field_optional_literal("result", side_elem(side_type_s64())),
	)
);

struct side_mpi_api_state_MPI_Wait {
	MPI_Status *status;
	struct side_mpi_unique_id id;
	int has_ret;
	int ret;
};

static inline void enter_MPI_Wait(struct side_mpi_api_state_MPI_Wait *side_state,
				  MPI_Request *request, MPI_Status *status)
{
	side_state->status = status;

	side_mpi_id_generator_next_id(&side_state->id);

	side_event(side_event_enter_MPI_Wait,
		side_arg_list(
			side_arg_u64(side_state->id.thread_id),
			side_arg_u64(side_state->id.local_id),
		)
	);
}

static inline void exit_MPI_Wait(struct side_mpi_api_state_MPI_Wait *side_state)
{
	if (side_event_enabled(side_event_exit_MPI_Wait)) {
		side_arg_define_optional(result, side_arg_s64(side_state->ret),
					 side_state->has_ret ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_event(side_event_exit_MPI_Wait,
			side_arg_list(
				side_arg_u64(side_state->id.thread_id),
				side_arg_u64(side_state->id.local_id),
				side_arg_gather_struct(side_state->status),
				side_arg_optional(result),
			)
		);
	}
}
