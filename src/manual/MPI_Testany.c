#include "common.h"

side_static_event(side_event_enter_MPI_Testany, "mpi", "enter_MPI_Testany", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_int("requests-count"),
	)
);

side_static_event(side_event_exit_MPI_Testany, "mpi", "exit_MPI_Testany", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("side-thread-id"),
		side_field_u64("side-local-id"),
		side_field_optional("status", side_mpi_maybe_status),
		side_field_optional_literal("result", side_elem(side_type_s64())),
	)
);

struct side_mpi_api_state_MPI_Testany {
	MPI_Status *status;
	int *flag;
	struct side_mpi_unique_id id;
	int has_ret;
	int ret;
};

static inline void enter_MPI_Testany(struct side_mpi_api_state_MPI_Testany *side_state,
				     int count, MPI_Request *requests,
				     int *index, int *flag, MPI_Status *status)
{
	side_state->status = status;
	side_state->flag   = flag;

	side_mpi_id_generator_next_id(&side_state->id);

	side_event(side_event_enter_MPI_Testany,
		side_arg_list(
			side_arg_u64(side_state->id.thread_id),
			side_arg_u64(side_state->id.local_id),
			side_arg_int(count),
		)
	);
}

static inline void exit_MPI_Testany(struct side_mpi_api_state_MPI_Testany *side_state)
{
	if (side_event_enabled(side_event_exit_MPI_Testany)) {
		side_arg_define_optional(status, side_arg_gather_struct(side_state->status),
					 *side_state->flag ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_arg_define_optional(result, side_arg_s64(side_state->ret),
					 side_state->has_ret ? SIDE_OPTIONAL_ENABLED : SIDE_OPTIONAL_DISABLED);
		side_event_call(side_event_exit_MPI_Testany,
			   side_arg_list(
				   side_arg_u64(side_state->id.thread_id),
				   side_arg_u64(side_state->id.local_id),
				   side_arg_optional(status),
				   side_arg_optional(result),
			   )
		);
	}

}
