#ifndef SIDE_MPI_MANUAL_COMMON
#define SIDE_MPI_MANUAL_COMMON

#include <side/trace.h>

/*
 * Use for MPI_Wait* and MPI_Test*
 */
static side_define_struct(side_mpi_status_struct_def,
	side_field_list(
		side_field_gather_signed_integer("source", offsetof(MPI_Status, MPI_SOURCE),
						 side_struct_field_sizeof(MPI_Status, MPI_SOURCE),
						 0, 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
		side_field_gather_signed_integer("tag", offsetof(MPI_Status, MPI_TAG),
						 side_struct_field_sizeof(MPI_Status, MPI_TAG),
						 0, 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
		side_field_gather_signed_integer("error", offsetof(MPI_Status, MPI_ERROR),
						 side_struct_field_sizeof(MPI_Status, MPI_ERROR),
						 0, 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
	)
);

/*
 * Use for MPI_Test*
 */
static side_define_optional(side_mpi_maybe_status,
	side_elem(
		side_type_gather_struct(side_mpi_status_struct_def,
					0, sizeof(MPI_Status),
					SIDE_TYPE_GATHER_ACCESS_DIRECT)
	)
);

static side_define_optional(side_mpi_maybe_statuses,
	side_elem(
		side_type_gather_vla(side_elem(side_type_gather_struct(side_mpi_status_struct_def,
								       0, sizeof(MPI_Status),
								       SIDE_TYPE_GATHER_ACCESS_DIRECT)),
				     0, SIDE_TYPE_GATHER_ACCESS_DIRECT,
				     side_length(side_type_gather_signed_integer(0, sizeof(int),
										 0, 0,
										 SIDE_TYPE_GATHER_ACCESS_DIRECT)))
	)
);

#endif
