<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# lttng-ust-mpi

Auto generation of instrumentation library for MPI with LTTng.

# Build

See [Build](doc/build.md)

# Usage

See [Usage](doc/usage.md)

# Tutorial

See [Tutorial](doc/tutorial.md)

# Set of ignored functions

The only function ignored is `MPI_Pcontrol`.  One can extend the `forbiden_list`
in `lttng-auto-mpi-wrappers` to ignore more functions.

# Development

If using the `guix` package manager, simply do `./dev-env`.
