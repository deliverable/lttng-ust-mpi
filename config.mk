# The provider of the events.
#
# By default, the provider is `mpi'.
PROVIDER?=mpi

# The MPI header to use.
#
# By default search for the header file for OpenMPI using pkg-config.
MPI_HEADER?=$(shell pkg-config --variable=includedir ompi)/mpi.h

builddir=$(CURDIR)/build
