# SPDX-License-Identifier: MIT
#
# SPDX-FileCopyrightText: 2023 Olivier Dion <odion@efficios.com>

include config.mk

PKG_CONFIG_PATH := $(builddir)/lib/pkgconfig:$(PKG_CONFIG_PATH)

LIBSIDE_CFLAGS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --cflags libside)
LIBSIDE_LIBS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --libs libside)

LTTNG_CFLAGS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --cflags lttng-ust)
LTTNG_LIBS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --libs lttng-ust)

# Do not modify below.
EXTRA_CFLAGS=$(LIBSIDE_CFLAGS) $(LTTNG_CFLAGS) -I include -Wno-deprecated-declarations $(CFLAGS)
EXTRA_LIBS=$(LIBSIDE_LIBS) $(LTTNG_LIBS) -ldl $(LDFLAGS)

AUTOGEN_MPI_API=		       \
	$(builddir)/libside-mpi-defs.h    \
	$(builddir)/libside-mpi-classes.h \
	$(builddir)/libside-mpi-states.h

MPI_MANUAL =                          \
	src/manual/MPI_Test.c         \
	src/manual/MPI_Testall.c      \
	src/manual/MPI_Testany.c      \
	src/manual/MPI_Testsome.c     \
	src/manual/MPI_Wait.c         \
	src/manual/MPI_Waitall.c      \
	src/manual/MPI_Waitany.c      \
	src/manual/MPI_Waitsome.c

all: $(builddir) $(builddir)/libside-mpi.so $(builddir)/test-mpi

$(builddir):
	mkdir -p $(builddir)

clean:
	rm -f $(builddir)/*
	rm -rf traces

check: all
	builddir=$(builddir) PROVIDER=$(PROVIDER) ./scripts/check

$(builddir)/test-mpi: tests/test-mpi.c
	mpicc -o $@ $^

$(builddir)/libside-mpi.so: $(builddir)/libside-mpi.c | $(AUTOGEN_MPI_API)
	gcc -O2 -ggdb3 -I $(builddir)/include -I include -I $$(dirname $(MPI_HEADER)) -Wall -Werror -shared -fPIC $(EXTRA_CFLAGS) -Wl,-rpath=$(builddir)/lib -o $@ $< $(EXTRA_LIBS)

$(builddir)/libside-mpi-manual.h: $(MPI_MANUAL)
	cat $^ > $@

$(builddir)/libside-mpi.c: $(MPI_HEADER) $(builddir)/libside-mpi-manual.h | scripts/side-auto-mpi-wrappers
	scripts/side-auto-mpi-wrappers $(MPI_HEADER) $@

$(AUTOGEN_MPI_API) &: $(MPI_HEADER) | scripts/side-auto-api
	scripts/side-auto-api --namespace side_mpi --ignores src/ignores.txt --provider $(PROVIDER) --emulated-classes --common-prefix MPI_ $^ $(AUTOGEN_MPI_API)

dist:
	git archive --prefix side-mpi/ --format=tar.gz --output side-mpi.tar.gz HEAD

.PHONY: check dist
